This is the home of the PlayLogix HTML boiler plate project, codenamed Chen.

It includes all of the libraries and HTML stack that we have grown accustomed too. The idea is to further generify this
project over time until the point where we can in the future spin up new HTML projects very rapidly.
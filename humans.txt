# The humans responsible & technology colophon

# TEAM
    Developer:  Abdul Rahim Allana
    Location:   Cape Town, South Africa

    Developer &
    Project Management:  Dean Boonzaier
    Location:            Cape Town, South Africa

# THANKS
    To the office coffee machine that worked tirelessly to ensure we were always well caffeinated.

# TECHNOLOGY COLOPHON
    Language:   English
    IDE:        IntelliJ IDEA 12, Chrome Web Developer Tools
    Standards:  HTML5, CSS3, Javascript
    Components: Bootstrap, Normalize, jQuery and various jQuery plugins, Modernizr

KNOWLEDGE IS POWER!
THIS SYSTEM ALLOWS IT'S USERS TO GAIN KNOWLEDGE
ERGO THIS SITE IS POWER, HOO-HAH!
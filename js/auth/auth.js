/**
 * User: Abdul Allana, Dean Boonzaier
 */
function PLAuth() {
    var AUTHORIZATION_HEADER_TEMPLATE = "{0} {1}:{2}";
    var AUTHORIZATION_STRING_TEMPLATE = "{0}\n{1}\n\n{2}\n{3}";
    var ERROR_TEMPLATE = "{0} must be specified in order to make authenticated requests";
    var userId;
    var accessKey;

    PLAuth.prototype.generateAuthorizationHeader = function (method, contentType, date, url) {
        if (isEmptyString(config.getApplicationName())) {
            throw new Error(ERROR_TEMPLATE.format("applicationName"));
        } else if (!plAuth.hasAccessKey()) {
            throw new Error(ERROR_TEMPLATE.format("accessKey"));
        } else if (!plAuth.hasUserId()) {
            throw new Error(ERROR_TEMPLATE.format("userId"));
        }
        var authString = AUTHORIZATION_STRING_TEMPLATE.format(method, contentType, date.toUTCString(), url);
        var encryptedAuthString = CryptoJS.HmacSHA1(authString, accessKey);
        var encryptedAuthBase64 = CryptoJS.enc.Base64.stringify(encryptedAuthString);
        var authorizationHeaderBody = AUTHORIZATION_HEADER_TEMPLATE.format(config.getApplicationName(), userId, encryptedAuthBase64);

        return authorizationHeaderBody;
    }

    PLAuth.prototype.setUserId = function(inputUserId) {
        userId = inputUserId;
    }

    PLAuth.prototype.getUserId = function() {
        return userId;
    }

    PLAuth.prototype.hasUserId = function() {
        return isNotEmptyString(userId);
    }

    PLAuth.prototype.setAccessKey = function(key) {
        accessKey = key;
    }

    PLAuth.prototype.getAccessKey = function() {
        return accessKey;
    }

    PLAuth.prototype.hasAccessKey = function() {
        return isNotEmptyString(accessKey);
    }

    PLAuth.prototype.encryptPassword= function(inputPassword) {
        var password = trimToNull(inputPassword);
        if (!password) {
            return null;
        }
        return CryptoJS.SHA1(password).toString(CryptoJS.enc.Base64);
    }
}

var plAuth = new PLAuth();
function initialize() {
    //Add String.format function
    if (!String.prototype.format) {
        String.prototype.format = function () {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number) {
                return typeof args[number] != 'undefined' ? args[number] : match;
            });
        };
    }
    //Add String.toTitleCase function
    String.prototype.toTitleCase = function () {
        var i, str, lowers, uppers;
        str = this.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });

        // Certain minor words should be left lowercase unless
        // they are the first or last words in the string
        lowers = ['A', 'An', 'The', 'And', 'But', 'Or', 'For', 'Nor', 'As', 'At',
            'By', 'For', 'From', 'In', 'Into', 'Near', 'Of', 'On', 'Onto', 'To', 'With'];
        for (i = 0; i < lowers.length; i++)
            str = str.replace(new RegExp('\\s' + lowers[i] + '\\s', 'g'),
                function (txt) {
                    return txt.toLowerCase();
                });

        // Certain words such as initialisms or acronyms should be left uppercase
        uppers = ['Id', 'Tv'];
        for (i = 0; i < uppers.length; i++)
            str = str.replace(new RegExp('\\b' + uppers[i] + '\\b', 'g'),
                uppers[i].toUpperCase());

        return str;
    }
    if (!window.JSON) {
        window.JSON = {
            parse: function (sJSON) { return eval("(" + sJSON + ")"); },
            stringify: function (vContent) {
                if (vContent instanceof Object) {
                    var sOutput = "";
                    if (vContent.constructor === Array) {
                        for (var nId = 0; nId < vContent.length; sOutput += this.stringify(vContent[nId]) + ",", nId++);
                        return "[" + sOutput.substr(0, sOutput.length - 1) + "]";
                    }
                    if (vContent.toString !== Object.prototype.toString) {
                        return "\"" + vContent.toString().replace(/"/g, "\\$&") + "\"";
                    }
                    for (var sProp in vContent) {
                        sOutput += "\"" + sProp.replace(/"/g, "\\$&") + "\":" + this.stringify(vContent[sProp]) + ",";
                    }
                    return "{" + sOutput.substr(0, sOutput.length - 1) + "}";
                }
                return typeof vContent === "string" ? "\"" + vContent.replace(/"/g, "\\$&") + "\"" : String(vContent);
            }
        };
    }
    // Setup Handlebars
    Handlebars.registerHelper('isEmptyArray', function (array, options) {
        if (isEmptyOrNullArray(array)) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
    Handlebars.registerHelper('isNotEmptyArray', function (array, options) {
        if (isNotEmptyOrNullArray(array)) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
    Handlebars.registerHelper(
        'appVersion',
        function () {
            return config.getApplicationVersion();
        }
    );
}

// Handlebars functions
Handlebars.getTemplate = function (name) {
    if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
        $.ajax({
            url: appendAppVersionNumberToURI('templates/' + name + '.handlebars'),
            success: function (data) {
                if (Handlebars.templates === undefined) {
                    Handlebars.templates = {};
                }
                Handlebars.templates[name] = Handlebars.compile(data);
            },
            async: false
        });
    }
    return Handlebars.templates[name];
}

function insertHandlebarsTemplate(templateName, templateData, targetElemId) {
    var template = Handlebars.getTemplate(templateName);
    //Generate some HTML code from the compiled Template
    var templateHTML = template(templateData);
    //Replace the body section with the new code.
    $("#" + targetElemId).html(templateHTML);
}

function getQueryParams(qs) {
    if (arguments.length == 0) {
        qs = document.location.search;
    }
    qs = qs.split("+").join(" ");
    var params = {}, tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }
    return params;
}

function showSuccessNotification(message) {
    $.notify(message, {className: "success", globalPosition: "top"});
}

function showErrorNotification(message) {
    $.notify(message, {className: "error", globalPosition: "top"});
}

function navigateTo(inputUrl) {
    var url = trimToNull(inputUrl);
    if (url) {
        if (arguments && arguments.length > 1) {
            var argsArray = Array.prototype.slice.call(arguments);
            argsArray.shift();
            url = url.format.apply(url, argsArray);
        }
        var baseAppDir = trimToUndefined(config.getApplicationBaseDir());
        if (baseAppDir) {
            url = baseAppDir + url;
        }
        window.location.href = url;
    }
}

function showLoadingIndicator() {
    var loaderTemplate = Handlebars.getTemplate("modalLoading");
    $("body").append(loaderTemplate);
    $(".modalLoadingIndicator").activity({width: 4});
}

function hideLoadingIndicator() {
    $(".modalLoading,.modalLoadingIndicator").remove();
}

function appendAppVersionNumberToURI(inputUri) {
    var uri = trimToNull(inputUri);
    if (!uri) {
        return null;
    }
    return updateQueryStringParameter(uri, "v", config.getApplicationVersion());
}

function updateQueryStringParameter(uri, paramName, paramVal) {
    var regExp = new RegExp("([?|&])" + paramName + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(regExp)) {
        return uri.replace(regExp, '$1' + paramName + "=" + paramVal + '$2');
    } else {
        return uri + separator + paramName + "=" + paramVal;
    }
}

// Array functions
function toArray(object) {
    if (Array.isArray(object)) {
        return object;
    } else if (!object) {
        return [];
    } else {
        return [object];
    }
}

function removeFromArray(array, value) {
    if (isEmptyOrNullArray(array)) {
        return array;
    }
    for (var i = 0; i < array.length; i++) {
        if (array[i] == value) {
            array.splice(i, 1);
        }
    }
    return array;
}

function arrayContains(array, value) {
    if(isEmptyOrNullArray(array)) {
        return false;
    }
    return array.indexOf(value) != -1;
}

function containsObjInArray(array, object, property) {
    if (isEmptyOrNullArray(array)) {
        return false;
    }
    for (var i = 0; i < array.length; i++) {
        if (array[i][property] == object[property]) {
            return true;
        }
    }
    return false;
}

function removeObjFromArray(array, property, propertyValue) {
    if (isEmptyOrNullArray(array)) {
        return array;
    }
    for (var i = 0; i < array.length; i++) {
        if (array[i][property] == propertyValue) {
            array.splice(i, 1);
        }
    }
    return array;
}

function findObjInArray(array, property, propertyValue) {
    if (isEmptyOrNullArray(array)) {
        return null;
    }
    for (var i = 0; i < array.length; i++) {
        if (array[i][property] == propertyValue) {
            return array[i];
        }
    }
    return null;
}

function isEmptyOrNullArray(array) {
    if (!Array.isArray(array)) {
        return true;
    }
    return array.length < 1;
}

function isNotEmptyOrNullArray(array) {
    return !isEmptyOrNullArray(array);
}

function getArrayElementAt(array, index) {
    if (isEmptyOrNullArray(array)) {
        return null;
    }
    if ((index < 0) || (index > array.length)) {
        return null;
    }
    return array[index];
}

// String functions
function isEmptyString(inputString) {
    return trimToNull(inputString) == null;
}

function isNotEmptyString(inputString) {
    return !isEmptyString(inputString);
}

function trimToValue(stringToTrim, returnValueOnEmptyOrNullString) {
    if (!stringToTrim) {
        return returnValueOnEmptyOrNullString;
    }
    var returnStr = $.trim(stringToTrim);
    return (returnStr.length == 0) ? returnValueOnEmptyOrNullString : returnStr;
}

function trimToNull(stringToTrim) {
    return trimToValue(stringToTrim, null);
}

function trimToUndefined(stringToTrim) {
    return trimToValue(stringToTrim, undefined);
}

function trimToEmpty(stringToTrim) {
    return trimToValue(stringToTrim, "");
}

initialize();
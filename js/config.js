function Config() {
    var CONFIG_FILE_PATH = "resources/config.json";
    var configLoaded = false;
    var configData;
    var configURL;

    function initialize() {
        configURL = CONFIG_FILE_PATH + "?" + Math.random(); // cache buster query string param
        $.ajax({
                url: configURL,
                dataType: 'json',
                async: false,
                success: onConfigLoadSuccess,
                error: onConfigLoadError
            }
        );
    }
    initialize();

    function onConfigLoadSuccess(data) {
        configData = data;
        configLoaded = true;
    }

    function onConfigLoadError(jqxhr, textStatus, error) {
        if (window.console) {
            console.log('Request Failed: ' + textStatus + ', ' + error);
        }
        alert("Configuration file failed to load from: " + configURL); // Using alert here as the popup javascript library probably hasn't been loaded yet
    }

    function getConfigFileProperty(propertyName) {
        if ((!configLoaded) || (!propertyName) || (!configData.hasOwnProperty(propertyName))) {
            return null;
        }
        return configData[propertyName];
    }

    Config.prototype.getBaseServiceURL = function () {
        return getConfigFileProperty('baseServiceURL');
    }

    Config.prototype.isInProductionMode = function () {
        // Default mode is production mode, so unless the appMode configuration property is explicitly set to debug we are always in production mode
        return !Config.isInDebugMode();
    }

    Config.prototype.isInDebugMode = function () {
        return (getConfigFileProperty('appMode') == "debug");
    }

    Config.prototype.getApplicationBaseDir = function () {
        var baseDir = getConfigFileProperty('applicationBaseDir');
        if (!baseDir) {
            return "/";
        }
        return baseDir;
    }

    Config.prototype.getApplicationVersion = function () {
        return getConfigFileProperty('version');
    }

    Config.prototype.getApplicationName = function () {
        return getConfigFileProperty('applicationName');
    }
}
var config = new Config();
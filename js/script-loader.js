function ScriptLoader() {
    var LIBRARIES = [
        //NOTE: Jquery must be the first library to be loaded to make progress updates to the ui easier to perform using jquery.
        "js/vendor/jquery-1.11.0.min.js",
        "//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js",
        "js/util/handlebars.min.js",
        "js/config.js",
        "js/common.js",
        "js/auth/auth.js",
        "js/auth/md5.js",
        "js/auth/hmac-sha1.js",
        "js/auth/enc-base64-min.js",
        "js/services.js",
        "js/util/jquery.validate.min.js",
        "js/util/jquery.activity-indicator-1.0.1.min.js",
        "js/util/notify.min.js"
    ];

    // Append console methods if they don't yet exist, added here so that console functions can safely be invoked at the earliest point in time
    (function() {
        var method;
        var noop = function () {};
        var methods = [
            'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
            'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
            'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
            'timeStamp', 'trace', 'warn'
        ];
        var length = methods.length;
        var console = (window.console = window.console || {});
        while (length--) {
            method = methods[length];

            // Only stub undefined methods.
            if (!console[method]) {
                console[method] = noop;
            }
        }
    }());

    var libsLoadedCount = 0;
    var totalLibsCount = LIBRARIES.length;
    var finalCompleteHandler;

    function fromPercentageString() {
        return Math.floor(libsLoadedCount * 100 / totalLibsCount) + "%";
    }

    function loadLibraries(progressHandler, completeHandler) {
        if (libsLoadedCount < LIBRARIES.length) {
            loadLibrary(LIBRARIES[libsLoadedCount], function () {
                libsLoadedCount++;
                if (progressHandler) {
                    progressHandler(fromPercentageString());
                }
                loadLibraries(progressHandler, loadLibraries);
            });
        } else {
            if (progressHandler) {
                progressHandler(fromPercentageString());
            }
            finalCompleteHandler();
        }
    }

    function loadLibrary(library, complete, additionalLibraries) {
        LazyLoad.js(
            library,
            function(context, event) {
                if (event && event.type == "error") {
                    if (console) {
                        console.error("Failed to load script: " + library);
                    }
                }
                if (complete) {
                    complete();
                }
            }
        );
    }

    ScriptLoader.prototype.loadScripts = function loadScripts(progressHandler, completeHandler, additionalLibraries) {
        if(additionalLibraries && additionalLibraries.length > 0) {
            LIBRARIES = LIBRARIES.concat(additionalLibraries);
            totalLibsCount = LIBRARIES.length;
        }

        finalCompleteHandler = completeHandler;
        loadLibraries(progressHandler, completeHandler);
    }
}
var ScriptLoader = new ScriptLoader();
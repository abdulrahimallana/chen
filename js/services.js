// This service has been setup in such a way that it makes requests to a RESTful api and deals with the standard response object we use
function Service() {
    var CONFIG_FILE_PATH = "resources/config.json";
    //var GET_REQUEST_PATH_EXAMPLE = "users";

    var JSON_RESPONSE_STATUSES = (function () {
        var private = {
            '0': 'STATUS_OK',
            '1': 'STATUS_API_ERROR',
            '2': 'STATUS_USER_ERROR',
            '3': 'STATUS_SYSTEM_ERROR'
        };
        return {
            get: function (statusId) {
                return private[statusId];
            }
        };
    })();
    var CONTENT_TYPE = "application/json; charset=UTF-8";

    var configLoaded = false;
    var configData = new Object;

    function initialize() {
        $.ajax(
            {
                url: CONFIG_FILE_PATH,
                dataType: 'json',
                async: false,
                success: onConfigLoadSuccess,
                error: onConfigLoadError
            }
        );
        // Enabled for IE
        $.support.cors = true;
    }

    initialize();

    function onConfigLoadSuccess(data) {
        configData = data;
        configLoaded = true;
    }

    function onConfigLoadError(jqxhr, textStatus, error) {
        console.log('Request Failed: ' + textStatus + ', ' + error);
        showSuccessNotification("Configuration file failed to load from: " + CONFIG_FILE_PATH);
    }

    function appendCacheBuster(url) {
        var cacheBuster = "cachebuster=" + new Date().getMilliseconds();
        if(url.indexOf("?") == -1) {
            url += "?" + cacheBuster;
        } else {
            url += "&" + cacheBuster;
        }
    }

    function performGetJSON(urlPart, successHandler, errorHandler, authenticated) {
        var requestURL = config.getBaseServiceURL() + urlPart;
        appendCacheBuster(requestURL);
        if(((!$.support.ajax) || (!window.XMLHttpRequest)) && (window.XDomainRequest)) {
            if(authenticated) {
                throw new Error("XDomain requests do not support custom headers. Can not execute authenticated requests.");
            }
            console.log("XDR requestURL: " + requestURL);
            var xdr = new XDomainRequest();
            xdr.open("get", requestURL);
            xdr.onprogress = function () { };
            xdr.ontimeout = function () { };
            xdr.onload = function() {
                standardSuccessHandler(
                    JSON.parse(xdr.responseText),
                    successHandler,
                    errorHandler
                )
            }
            xdr.onerror = function () {
                if (errorHandler) {
                    errorHandler({
                        status: "Unknown",
                        errorMessage: "XDR Error"
                    });
                } else {
                    if (config.isInDebugMode()) {
                        showError(
                            "An error occurred while trying to get data. Status Code: Unknown, Message: XDR Error"
                        );
                    } else {
                        showError();
                    }
                }
            };
            setTimeout(function () {xdr.send();}, 0);
        } else {
            var requestOptions =  {dataType: "json", url: requestURL};
            requestOptions.beforeSend = function(request) {
                if(authenticated) {
                    appendAutheticationHeadersToRequest(request, "GET", requestURL);
                }
            };
            requestOptions.success = function (data) {
                standardSuccessHandler(
                    data,
                    successHandler,
                    errorHandler
                )
            };
            //TODO: Improve the non generic error handling for cases such as 404 responses
            requestOptions.fail = function (errorDetails) {
                if (errorHandler) {
                    errorHandler({
                        status: errorDetails.status,
                        errorMessage: errorDetails.statusText
                    });
                } else {
                    if (config.isInDebugMode()) {
                        showError(
                            "An error occurred while trying to get data. Status Code: {0}, Message: {1}".format(
                                errorDetails.status,
                                improveDebugErrorPopUpMessage(errorDetails)
                            )
                        );
                    } else {
                        showError();
                    }
                }
            };
            $.ajax(requestOptions);
        }
    }

    function performPostJSON(urlPart, jsonData, successHandler, errorHandler, authenticated) {
        var requestURL = config.getBaseServiceURL() + urlPart;
        if(((!$.support.ajax) || (!window.XMLHttpRequest)) && (window.XDomainRequest)) {
            if(authenticated) {
                throw new Error("XDomain requests do not support custom headers. Can not execute authenticated requests.");
            }
            console.log("XDR requestURL: " + requestURL);
            var xdr = new XDomainRequest();
            xdr.open("POST", requestURL);
            xdr.onprogress = function () { };
            xdr.ontimeout = function () { };
            xdr.onload = function() {
                standardSuccessHandler(
                    JSON.parse(xdr.responseText),
                    successHandler,
                    errorHandler
                )
            }
            xdr.onerror = function () {
                if (errorHandler) {
                    errorHandler({
                        status: "Unknown",
                        errorMessage: "XDR Error"
                    });
                } else {
                    if (config.isInDebugMode()) {
                        showError(
                            "An error occurred while trying to get data. Status Code: Unknown, Message: XDR Error"
                        );
                    } else {
                        showError();
                    }
                }
            };
            setTimeout(
                function () {
                    xdr.send(JSON.stringify(jsonData));
                },
                0
            );
        } else {
            $.ajax(
                {
                    type: "POST",
                    url: requestURL,
                    data: JSON.stringify(jsonData),
                    dataType: "json",
                    contentType: CONTENT_TYPE,
                    beforeSend: function(request) {
                        if(authenticated) {
                            appendAutheticationHeadersToRequest(request, "POST", requestURL);
                        }
                    },
                    success: function (data) {
                        standardSuccessHandler(
                            data,
                            successHandler,
                            errorHandler
                        )
                    },
                    fail: function (errorDetails) {
                        if (errorHandler) {
                            errorHandler(
                                {
                                    status: errorDetails.status,
                                    errorMessage: errorDetails.statusText
                                }
                            );
                        } else {
                            if (config.isInDebugMode()) {
                                showError(
                                    "An error occurred while trying to get data. Status Code: {0}, Message: {1}".format(
                                        errorDetails.status,
                                        improveDebugErrorPopUpMessage(errorDetails)
                                    )
                                );
                            } else {
                                showError();
                            }
                        }
                    }
                }
            );
        }
    }

    function appendAutheticationHeadersToRequest(request, requestMethod, requestURL) {
        var authDate = new Date();
        var authHeader = plAuth.generateAuthorizationHeader(requestMethod, CONTENT_TYPE, authDate, requestURL);
        request.setRequestHeader("authorization", authHeader);
        request.setRequestHeader("x-date", authDate.toUTCString());
    }

    function showError(message) {
        // If we have a custom error message display that, otherwise display the default message.
        if (message) {
            showErrorNotification(message);
        } else {
            showErrorNotification("We're sorry but we're having trouble fetching data.");
        }
    }

    function improveDebugErrorPopUpMessage(errorDetails) {
        if ((!errorDetails) || (!errorDetails.hasOwnProperty("status"))) {
            return null;
        }
        // Assume a status code of 0 means that no connection could be established with the remote service, hence the invalid response code.
        if (errorDetails.status == 0) {
            return "Unable to connect to server";
        }
        return errorDetails.statusText;
    }

    function processJSONResponse(jsonResponse, successHandler, faultHandler) {
        if (!jsonResponse) {
            console.log("cannot process a null jsonResponse, " + jsonResponse);
            return null;
        }
        if (JSON_RESPONSE_STATUSES.get(jsonResponse.status) == "STATUS_OK") {
            if (jsonResponse.details) {
                if (successHandler) {
                    successHandler(jsonResponse.details);
                }
                return jsonResponse.details;
            }
            else if (successHandler) {
                successHandler();
            }
        } else {
            if (faultHandler) {
                faultHandler(jsonResponse);
            } else {
                if (config.isInDebugMode()) {
                    var errorMessage = "An error occurred - Status: {0}, Message: {1}, Error Code: {2}".format(JSON_RESPONSE_STATUSES.get(jsonResponse.status), jsonResponse.errorMessage, jsonResponse.errorCode);
                    console.log(errorMessage);
                    showError(errorMessage);
                } else {
                    showError();
                }
            }
        }
        return null;
    }

    function standardSuccessHandler(jsonResponse, successHandler, faultHandler) {
        processJSONResponse(
            jsonResponse,
            successHandler,
            faultHandler
        );
    }

    /*Service.prototype.exampleGetRequest = function (username, successHandler, errorHandler) {
        performJSONPost(GET_REQUEST_PATH_EXAMPLE.format(username), null, successHandler, errorHandler);
    }*/
}
var services = new Service();